document.addEventListener("DOMContentLoaded", function () {

    //zop.Components.Init();

});

if (zop == undefined)
{
    var zop = {};
}


zop.DataSource = (function () {

    function Init() {

    }

    function GetListOnDay(date) {
        var dataSource = [];

        for(i = 1; i < 10; i++){

            let row = {
                Id: i,
                FIO: "Иванов И.И.",
                Z: true,
                O: false,
                P: false
            };

            dataSource.push(row);
        }

        return dataSource;
    }

    return {
        Init: Init,
        GetListOnDay: GetListOnDay
    };

})();


zop.Components = (function () {

    var mainTable;

    function Init() {
        InitMainTable();
    }

    function MainTable() { return mainTable; }

    function InitMainTable(dataSource) {

        mainTable = $('#mainTable');
        mainTable.children('tbody').empty();

        FillMainTable();
    }

    function FillMainTable(date){

        var dataSource = zop.DataSource.GetListOnDay(date);

        dataSource.map(function(row) {
            let html = '<tr data-id="' + row.Id +'">';
            html += '<td>' + row.FIO + '</td>';
            html += '<td><input type="checkbox" name="checkbox" id="cb-' + row.Id +'-z" ></td>';
            html += '<td><input type="checkbox" name="checkbox-' + row.Id +'-o" id="cb-' + row.Id +'-o" data-cacheval="' + row.O + '"></td>';
            html += '<td><input type="checkbox" name="checkbox-' + row.Id +'-p" id="cb-' + row.Id +'-p" data-cacheval="' + row.P + '"></td>';
            html += '</tr>';

            //$("fieldset").append('<input type="checkbox" name="' + name + '" id="id' + i + '"><label for="id' + i + '">' + name + ' ' + i + '</label>');
            mainTable.children('tbody').append(html);
        });

    }

    return {
        Init: Init,

        MainTable: MainTable,
        FillMainTable: FillMainTable
    };

})();
